package com.themelt.owo

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.microsoft.identity.client.*
import com.microsoft.identity.client.PublicClientApplication
import android.arch.lifecycle.MutableLiveData





class AppSubClass(application: Application) : AndroidViewModel(application) {

    var sampleApp: PublicClientApplication? = null

    private lateinit var authResult: MutableLiveData<AuthenticationResult>

    private lateinit var watsonUsername: String
    private lateinit var watsonPassword: String
    private lateinit var watsonWorkspaceId: String
    private lateinit var sleeping: MutableLiveData<Boolean>

    fun getWatsonUsername(): String{
        if (!::watsonUsername.isInitialized) {
            watsonUsername= (getApplication() as Application).applicationContext.getString(R.string.conversation_username)
        }
        return watsonUsername
    }

    fun getWatsonPassword(): String{
        if (!::watsonPassword.isInitialized) {
            watsonPassword= (getApplication() as Application).applicationContext.getString(R.string.conversation_password)
        }
        return watsonPassword
    }

    fun getWatsonWorkspaceId(): String{
        if (!::watsonWorkspaceId.isInitialized) {
            watsonWorkspaceId= (getApplication() as Application).applicationContext.getString(R.string.workspace_id)
        }
        return watsonWorkspaceId
    }

    fun getAuthObserver(): MutableLiveData<AuthenticationResult> {
        if (!::authResult.isInitialized) {
            authResult = MutableLiveData()
        }
        return authResult
    }

    fun setAuthResult(authResult: AuthenticationResult) {
        getAuthObserver().value = authResult
    }

    fun getAuthResult(): AuthenticationResult? {
        return getAuthObserver().value
    }

    fun getSleepingObserver(): MutableLiveData<Boolean> {
        if (!::sleeping.isInitialized) {
            sleeping = MutableLiveData()
            sleeping.value=false
        }
        return sleeping
    }

    fun setSleeping(sleeping: Boolean) {
        getSleepingObserver().value = sleeping
    }

    fun getSleeping(): Boolean {
        return getSleepingObserver().value!!
    }



}