package com.themelt.owo.fragments

import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.themelt.owo.R
import com.themelt.owo.bot.BotDialog


class Pensum : Fragment() {

    val TAG= "Pensum"

    companion object {
        fun newInstance() = Pensum()
    }

    private lateinit var viewModel: PensumViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.pensum_fragment, container, false)

        val owoFab= view.findViewById<FloatingActionButton>(R.id.owo_fab)

        owoFab.setOnClickListener {
            Log.d(TAG, "onClick: opening dialog")

            val dialog = BotDialog()

            val args = Bundle()
            args.putString("initFragment", "Pensum")
            dialog.arguments = args

            dialog.setTargetFragment(this@Pensum, 1)
            dialog.show(fragmentManager, "OwO Dialog")
            this.fragmentManager?.executePendingTransactions()

            val d = dialog.dialog
            d.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PensumViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
