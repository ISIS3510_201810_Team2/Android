package com.themelt.owo.utils

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.prolificinteractive.materialcalendarview.CalendarDay
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TaskQueriesSingleton constructor(context: Context): ConnectivityManager.NetworkCallback()  {
    companion object {
        @Volatile
        private var INSTANCE: TaskQueriesSingleton? = null
        fun getInstance(context: Context) =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: TaskQueriesSingleton(context).also {
                        INSTANCE = it
                    }
                }

    }
    val TAG = "TaskQueriesSingleton"

    val appContext: Context by lazy {
        context.applicationContext
    }

    val backendUrl = "https://fierce-ocean-10173.herokuapp.com/"

    fun addTask(task: Task, success: (Task) -> Unit, errorCall: (String) -> Unit = {
        s ->
        Toast.makeText(
                appContext,
                s,
                Toast.LENGTH_SHORT).show()
    }) {
        val url = backendUrl + "task"

        val parameters = JSONObject()
        val taskJson = JSONObject()
        try {
            parameters.put("user", "Test")
            taskJson.put("name", task.name)
            taskJson.put("date", task.date)
            taskJson.put("type", task.type)
            parameters.put("task", taskJson)
        } catch (e: Exception) {
        }
        val req: JsonObjectRequest = object : JsonObjectRequest(Request.Method.POST, url, parameters,
                Response.Listener<JSONObject> { response ->
                    success(task)
                },
                Response.ErrorListener { error ->
                    Log.d(TAG, error.toString())
                    errorCall("Hubo un error :/ intenta más tarde")
                }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //headers.put("Content-Type", "application/json");
                headers["Content-Type"] = "application/json"
                return headers
            }
        }
        RequestQueueSingleton.getInstance(appContext)
                .addToRequestQueue(req)
    }

    fun queryTasks(year: Int?, month: Int, day: Int?, success: (List<Task>) -> Unit, errorCall: (String) -> Unit = {
        s ->
        Toast.makeText(
                appContext,
                s, // "Error cargando las tareas. Revisa tu conexión."
                Toast.LENGTH_SHORT).show()
    }){
        var url = backendUrl + "task?month=$month"
        if (year != null && day != null) {
            url += "&year=$year&day=$day"
        }
        Log.e("QUERYTASKS", url)
        val req: JsonObjectRequest = object : JsonObjectRequest(url, null,
                Response.Listener<JSONObject> { response ->
                    val tasksRes = response.getJSONArray("tasks")
                    val tasksList = ArrayList<Task>()
                    for (i in 0..(tasksRes.length() - 1)) {
                        val item = tasksRes.getJSONObject(i)
                        val type = item.getString("type")
                        val name = item.getString("name")
                        val date = item.getString("date").split(".")[0]
                        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                        tasksList.add(Task(name, type, dateFormat.parse(date)))
                    }
                    success(tasksList)
                },
                Response.ErrorListener { error ->
                    Log.d(TAG, error.toString())
                    errorCall("Hubo un error (Revisar conexión a internet) :/ intenta más tarde")
                }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"
                return headers
            }
        }

        RequestQueueSingleton.getInstance(appContext).addToRequestQueue(req) {
            errorCall("No hay conexión")
        }
    }

    fun queryTaskDatesByMonth(month: Int, success: (List<Task>) -> Unit, errorCall: (String) -> Unit = {
        s ->
        Toast.makeText(
                appContext,
                s, // "Error cargando las tareas. Revisa tu conexión."
                Toast.LENGTH_SHORT).show()
    }) {
        queryTasks(null, month, null, success, errorCall)
    }

}

data class Task(val name: String, val type: String, val date: Date): Comparable<Task> {
    override fun compareTo(other: Task): Int = date.compareTo(other.date)
    fun timeString(): String {
        val cal = Calendar.getInstance()
        cal.time = date
        var timeString = cal.get(Calendar.HOUR_OF_DAY).toString() + ":"
        var minute = cal.get(Calendar.MINUTE).toString()
        if (minute.length == 1) {
            minute = "0$minute"
        }
        timeString += minute
        return timeString
    }
    fun dateString(): String {
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        sdf.timeZone = cal.timeZone
        return sdf.format(date)
    }

    fun description(): String = "${dateString()}: Tarea de tipo $type"
}