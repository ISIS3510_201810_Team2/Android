package com.themelt.owo.adapters

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.database.Cursor
import android.provider.CalendarContract
import android.view.View
import com.themelt.owo.R
import android.view.LayoutInflater
import android.view.ViewGroup
import java.text.SimpleDateFormat


class EventCursorAdapter: RecyclerView.Adapter<EventCursorAdapter.VH>() {

    private var mCursor: Cursor? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.task_quickview, parent, false)
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        mCursor!!.moveToPosition(position)
        val idxName= mCursor!!.getColumnIndex(CalendarContract.Events.TITLE)
        val idxDescr = mCursor!!.getColumnIndex(CalendarContract.Events.DESCRIPTION)
        val idxStart = mCursor!!.getColumnIndex(CalendarContract.Events.DTSTART)

        val name = mCursor!!.getString(idxName)
        val description = mCursor!!.getString(idxDescr)
        val startDate = mCursor!!.getLong(idxStart)
        val time = timeToString(startDate)
        holder.nameText.text = name
        holder.descriptionText.text = description
        holder.timeText.text = time
    }

    override fun getItemCount(): Int {
        return mCursor?.count ?: 0
    }

    override fun getItemId(position: Int): Long {
        if (mCursor != null && mCursor!!.moveToPosition(position)) {
            val idxId = mCursor!!.getColumnIndex(CalendarContract.Events._ID)
            val id = mCursor!!.getLong(idxId)
            return id
        }
        return 0
    }

    private fun timeToString(timeMillis: Long): String {
        val dateFormat = SimpleDateFormat("dd-MM-yyyy, hh:mm a");
        return dateFormat.format(timeMillis)
    }

    fun getCursor(): Cursor? {
        return mCursor
    }

    fun setCursor(newCursor: Cursor) {
        mCursor = newCursor
        notifyDataSetChanged()
    }

    class VH(v: View) : RecyclerView.ViewHolder(v) {
        var timeText: TextView = v.findViewById<TextView>(R.id.task_quickview_time)
        var nameText: TextView = v.findViewById<TextView>(R.id.task_quickview_name)
        var descriptionText: TextView = v.findViewById<TextView>(R.id.task_quickview_desciption)
    }
}