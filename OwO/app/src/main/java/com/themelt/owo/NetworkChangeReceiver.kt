package com.themelt.owo


import android.net.ConnectivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NetworkChangeReceiver(val listener:ConectionListener) : BroadcastReceiver() {

    interface ConectionListener {
        fun onConectionChange(isOnline: Boolean)
    }

    override fun onReceive(context: Context, intent: Intent) {
        try {
            if (isOnline(context)) {
                listener.onConectionChange(true)
            } else {
                listener.onConectionChange(false)
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }

    private fun isOnline(context: Context): Boolean {
        try {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            //should check null because in airplane mode it will be null
            return netInfo != null && netInfo.isConnected
        } catch (e: NullPointerException) {
            e.printStackTrace()
            return false
        }

    }
}