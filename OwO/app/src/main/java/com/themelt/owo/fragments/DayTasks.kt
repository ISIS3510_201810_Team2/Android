package com.themelt.owo.fragments

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import com.themelt.owo.R
import com.themelt.owo.bot.BotDialog
import com.themelt.owo.utils.Task
import com.themelt.owo.utils.TaskQueriesSingleton


class DayTasks : Fragment() {

    val TAG= "Calendar"

    companion object {
        fun newInstance() = DayTasks()
    }

    private lateinit var viewModel: DayTasksViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.day_tasks_fragment, container, false)

        val backBtn  = view.findViewById<Button>(R.id.day_tasks_back_button)
        backBtn.setOnClickListener {
            fragmentManager?.popBackStack()
        }
        val args = arguments
        if (args != null) {
            val year = args.get("year") as Int
            val month = args.get("month") as Int
            val day = args.get("day") as Int
            TaskQueriesSingleton.getInstance(context!!).queryTasks(year.toInt(), month.toInt(), day.toInt(), {
                tasks: List<Task> ->
                showTasks(view, tasks, inflater)
                hideTasksLoading(view)
            }) {
                msg ->
                Log.e("Error", "msg")
                backBtn.performClick()
            }
        }
        return view
    }

    private fun hideTasksLoading(view: View) {
        val pb = view.findViewById<ProgressBar>(R.id.day_tasks_loading)
        pb.visibility = View.GONE
    }

    private fun showTasks(view: View, tasks: List<Task>, inflater: LayoutInflater) {
        val taskListView = view.findViewById<LinearLayout>(R.id.tasks_list_view)
        var taskView: View?
        if (tasks.isEmpty()) {
            taskView = inflater.inflate(R.layout.task_quickview, null)
            taskView.findViewById<TextView>(R.id.task_quickview_name).text = "No hay eventos para hoy"
            taskView.findViewById<TextView>(R.id.task_quickview_time).text = ""
            taskView.findViewById<TextView>(R.id.task_quickview_desciption).text = ""
            taskListView.addView(taskView)
        } else {
            for (task in tasks) {
                taskView = inflater.inflate(R.layout.task_quickview, null)
                taskView.findViewById<TextView>(R.id.task_quickview_time).text = task.timeString()
                taskView.findViewById<TextView>(R.id.task_quickview_name).text = task.name
                taskView.findViewById<TextView>(R.id.task_quickview_desciption).text = task.description()
                taskListView.addView(taskView)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DayTasksViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
