package com.themelt.owo.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.Volley
import com.android.volley.RequestQueue
import com.android.volley.Response
import java.util.*

class RequestQueueSingleton constructor(context: Context): ConnectivityManager.OnNetworkActiveListener {
    companion object {
        @Volatile
        private var INSTANCE: RequestQueueSingleton? = null
        fun getInstance(context: Context) =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: RequestQueueSingleton(context).also {
                        val connectivityManager = context.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                        connectivityManager.addDefaultNetworkActiveListener(it)
                        INSTANCE = it
                    }
                }
    }



    val requestQueue: RequestQueue by lazy {
        // applicationContext is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        Volley.newRequestQueue(context.applicationContext)
    }

    val requestsToRetryQueue: Queue<Any> = ArrayDeque()

    val myContext: Context by lazy {
        context.applicationContext
    }
    fun <T> addToRequestQueue(req: Request<T>, retryWhenConnected: Boolean = false, noConnectionMsg: String = "No hay conexión a internet :(",
                              noConnectionCallback: () -> Unit = {}) {
        if (!isConnected()) {
            noConnectionCallback()
            Toast.makeText(
                    myContext,
                    noConnectionMsg,
                    Toast.LENGTH_SHORT).show()
            if (retryWhenConnected) {
                requestsToRetryQueue.add(req)
            }
        } else {
            requestQueue.add(req)
        }
    }

    override fun onNetworkActive() {
        Log.e("NETWORK", "connected again")
        while (requestsToRetryQueue.isNotEmpty()) {
            if (isConnected()) {
                requestQueue.add(requestsToRetryQueue.poll() as Request<Any>)
            } else {
                break
            }
        }
    }

     fun isConnected(): Boolean {
        val cm = myContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnected == true
    }
}