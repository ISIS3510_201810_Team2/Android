package com.themelt.owo.fragments.campusAccess

import android.graphics.Bitmap
import android.os.AsyncTask
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import java.lang.ref.WeakReference


class QRGenerator(campusAccess: CampusAccess) : AsyncTask<String, Unit, Bitmap>() {

    private var mFragment: WeakReference<CampusAccess>? = null

    init {
        mFragment = WeakReference(campusAccess)
    }
    override fun doInBackground(vararg params: String): Bitmap? {
        val qrCode = params[0]
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(qrCode, BarcodeFormat.QR_CODE, 800, 800)
            val barcodeEncoder = BarcodeEncoder()
            return barcodeEncoder.createBitmap(bitMatrix)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
        return null
    }

    override fun onPostExecute(result: Bitmap?) {
        mFragment?.get()?.updateQRImage(result)
    }
}