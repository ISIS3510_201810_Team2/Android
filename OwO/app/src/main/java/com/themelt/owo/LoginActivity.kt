package com.themelt.owo

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.util.Log
import android.view.View
import com.microsoft.identity.client.*

import com.microsoft.identity.client.PublicClientApplication
import com.themelt.owo.main.MainActivity
import android.widget.ProgressBar
import android.widget.Toast
import android.net.ConnectivityManager
import android.graphics.Color
import android.net.Network
import android.net.NetworkRequest
import android.widget.TextView



class LoginActivity : AppCompatActivity(),NetworkChangeReceiver.ConectionListener {

    val TAG = "Login activity"
    val CLIENT_ID = "5301a170-53af-4a3f-a70a-a06a1dc9ff04"
    val SCOPES = arrayOf("https://graph.microsoft.com/User.Read")

    var mContext: Context? = null

    var loginButton: CardView? = null

    var conectionIndicator: CardView?=null

    var textConection: TextView?=null

    var myApp: PublicClientApplication?=null

    var primeraConexion= false

    lateinit var appViewModel:AppSubClass

    private var manager: ConnectivityManager? = null

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            // this ternary operation is not quite true, because non-metered doesn't yet mean, that it's wifi
            // nevertheless, for simplicity let's assume that's true
            runOnUiThread {
                onConectionChange(true)
            }
            val delayrunnable = Runnable {
                Log.v("se", "ejecuta")
                Log.v("sea", conectionIndicator.toString() )
                Log.v("sea", conectionIndicator?.layoutParams.toString())
                conectionIndicator?.visibility= View.GONE
            }
            conectionIndicator?.postDelayed(delayrunnable, 3000)
            Log.v("Al menos", "sniff")

        }

        override fun onLost(network: Network) {
            Log.v("Conexion", "llega mal")
            super.onLost(network)
            val connectivityManager = applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            val networkInfo = connectivityManager?.activeNetworkInfo
            if (networkInfo == null || !networkInfo.isConnected) {
                runOnUiThread {
                    onConectionChange(false)
                }
            }

        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginButton = findViewById(R.id.login_button)
        conectionIndicator= findViewById(R.id.conection_indicator)
        textConection= conectionIndicator?.findViewById(R.id.textConnection)


        mContext= this
        appViewModel= ViewModelProviders.of(this).get(AppSubClass::class.java)
        myApp = PublicClientApplication( this.applicationContext, CLIENT_ID)
        appViewModel.sampleApp= myApp
        loginButton?.setOnClickListener {
            findViewById<ProgressBar>(R.id.pbLogin).visibility= View.VISIBLE
            Log.v("Hol", "Llega")
            loginButton?.isEnabled = false
            myApp?.acquireToken(this, SCOPES, getAuthInteractiveCallback())

        }

        manager =  getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        manager?.registerNetworkCallback(NetworkRequest.Builder().build(),networkCallback)

        val connectivityManager = applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val networkInfo = connectivityManager?.activeNetworkInfo
        if (networkInfo == null || !networkInfo.isConnected) {
                onConectionChange(false)
        }
    }

    override fun onConectionChange(isOnline: Boolean) {
        if (isOnline) {
            Log.v("Conexion","hay")
            conectionIndicator?.layoutParams?.height=50
            conectionIndicator?.visibility= View.VISIBLE
            conectionIndicator?.setBackgroundColor(Color.GREEN)
            textConection?.text = getString(R.string.internet_connected)

        } else {
            Log.v("Conexion","noHay")
            conectionIndicator?.layoutParams?.height=50
            conectionIndicator?.visibility= View.VISIBLE
            conectionIndicator?.setBackgroundColor(Color.RED)
            textConection?.text = getString(R.string.internet_not_connected)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.v("Holi","Por fin")
        myApp?.handleInteractiveRequestRedirect(requestCode, resultCode, data)
    }

    fun getAuthInteractiveCallback(): AuthenticationCallback {
        return object : AuthenticationCallback {
            override fun onSuccess(authenticationResult: AuthenticationResult) {

                var sampleApp = appViewModel.sampleApp

                /* Initialize the MSAL App context */
                if (sampleApp == null) {
                    sampleApp = PublicClientApplication(
                            mContext!!,
                            CLIENT_ID)
                    appViewModel.sampleApp= sampleApp
                }
                Log.v("yei", "successss")
                appViewModel.setAuthResult(authenticationResult)
                val intent = Intent(mContext, MainActivity::class.java)
                startActivity(intent)
                finish()
            }

            override fun onError(exception: MsalException) {
                findViewById<ProgressBar>(R.id.pbLogin).visibility= View.GONE
                /* Failed to acquireToken */
                Log.v(TAG, "Authentication failed: " + exception.toString())
                loginButton?.isEnabled = true

                if (exception is MsalClientException) {
                    Toast.makeText(applicationContext, "No hay conexión a internet :/", Toast.LENGTH_LONG).show()
                } else if (exception is MsalServiceException) {
                    Toast.makeText(applicationContext, "Error de Microsoft, intente después :/", Toast.LENGTH_LONG).show()
                }
            }

            override fun onCancel() {
                findViewById<ProgressBar>(R.id.pbLogin).visibility= View.GONE
                /* User cancelled the authentication */
                Log.v(TAG, "User cancelled login.")
                loginButton?.isEnabled = true
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        primeraConexion=false
        manager?.unregisterNetworkCallback(networkCallback)
    }
}
