package com.themelt.owo.fragments

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.os.ResultReceiver
import android.os.SystemClock
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.themelt.owo.R
import com.themelt.owo.models.EventModel
import com.themelt.owo.services.OwoCalendarService
import java.util.*
import java.util.Calendar
import android.widget.Toast
import com.themelt.owo.utils.OwoEventManager


class CreateTaskDialog  : DialogFragment() {
    companion object {
        fun newInstance(): CreateTaskDialog {
            val f = CreateTaskDialog()
            return f
        }
    }

    private var myContext: Context? = null
    private var lastButtonMilli = SystemClock.elapsedRealtime()

    private var owoEventManager: OwoEventManager? = null

    override fun onDestroy() {
        super.onDestroy()
        myContext = null
        owoEventManager = null
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OwoEventManager) {
            owoEventManager = context as OwoEventManager
        } else {
            throw RuntimeException(context.toString() + " must implement OwoEventManager")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.create_task_dialog_fragment, container)
        myContext = activity!!.applicationContext
        val types_array = arrayAdapterFromId(R.array.task_types_array)
        val type_spinner = createSpinner(rootView, types_array, R.id.task_types_spinner)

        val years_array = arrayAdapterFromId(R.array.years_array)
        val year_spinner = createSpinner(rootView, years_array, R.id.task_year_spinner)

        val months_array = arrayAdapterFromId(R.array.months_array)
        val month_spinner = createSpinner(rootView, months_array, R.id.task_month_spinner)

        val days_array = arrayAdapterFromId(R.array.days_base_array)
        val days_spinner = createSpinner(rootView, days_array, R.id.task_day_spinner)

        val hours_array = arrayAdapterFromId(R.array.hours_array)
        val hours_spinner = createSpinner(rootView, hours_array, R.id.task_hour_spinner)

        val minutes_array = arrayAdapterFromId(R.array.minutes_array)
        val minutes_spinner = createSpinner(rootView, minutes_array, R.id.task_minutes_spinner)

        minutes_spinner.setSelection(Calendar.getInstance().get(Calendar.MINUTE))
        hours_spinner.setSelection(Calendar.getInstance().get(Calendar.HOUR_OF_DAY))
        val month = Calendar.getInstance().get(Calendar.MONTH)
        month_spinner.setSelection(Calendar.getInstance().get(Calendar.MONTH))
        val year = Calendar.getInstance().get(Calendar.YEAR)
        year_spinner.setSelection(year - 2000)


        updateDaysArray(year, month + 1, days_spinner)

        // Set an on item selected listener for spinner object
        val itemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                val year_value = year_spinner.selectedItemPosition + 2000
                val month_value = month_spinner.selectedItemPosition
                updateDaysArray(year_value, month_value + 1, days_spinner)
            }

            override fun onNothingSelected(parent: AdapterView<*>){
                // Another interface callback
            }
        }
        year_spinner.onItemSelectedListener = itemSelectedListener
        month_spinner.onItemSelectedListener = itemSelectedListener

        days_spinner.setSelection(Calendar.getInstance().get(Calendar.DAY_OF_MONTH) - 1)

        val closeBtn = rootView.findViewById<Button>(R.id.close_create_task_button)
        closeBtn.setOnClickListener {
            this.dismiss()
        }

        val addTaskErrorText = rootView.findViewById<TextView>(R.id.create_task_error_text)
        addTaskErrorText.visibility = View.GONE
        val addBtn = rootView.findViewById<Button>(R.id.add_task_button)
        addBtn.setOnClickListener {
            val currMilli = SystemClock.elapsedRealtime()
            if (currMilli - lastButtonMilli >= 1000) {
                lastButtonMilli = currMilli
                val name = rootView.findViewById<EditText>(R.id.task_name_input).text.toString()
                val year = year_spinner.selectedItemPosition + 2000
                val month = month_spinner.selectedItemPosition
                val day = days_spinner.selectedItemPosition
                val hour = hours_spinner.selectedItemPosition
                val minutes = minutes_spinner.selectedItemPosition
                val type = type_spinner.selectedItem.toString()
                val cal = Calendar.getInstance()
                cal.set(year, month, day + 1, hour, minutes)
                Log.e("ADD", name)
                Log.e("ADD", cal.time.toString())
                Log.e("ADD", type)
                if (name.equals("")) {
                    addTaskErrorText.text = "Completa todos los campos"
                    addTaskErrorText.visibility = View.VISIBLE
                } else {
                    val endcal = Calendar.getInstance()
                    endcal.timeInMillis = cal.timeInMillis
                    endcal.add(Calendar.HOUR_OF_DAY, 1);
                    val e = EventModel(title = name, description = type, startDate = cal, endDate = endcal)
                    owoEventManager?.addEvent(e, CreateTaskResponder())
                }
            }

        }
        return rootView
    }

    private fun updateDaysArray(year: Int, month: Int, days_spinner: Spinner) {
        var isLeapYear = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)
        var add = 0
        if(isLeapYear) {
            add = 1
        }
        var daysInMonth = 31 - (month - 1) % 7 % 2
        if (month == 2) {
            daysInMonth = 28 + add
        }
        val iter = (1..daysInMonth).iterator()
        var arr = ArrayList<CharSequence>()
        for (num in iter) {
            if (num <= 9) {
                arr.add("0" + num)
            } else {
                arr.add("" + num)
            }
        }
        days_spinner.adapter = ArrayAdapter<CharSequence>(myContext, android.R.layout.simple_spinner_item, arr)

    }

    private fun arrayAdapterFromId(array_type: Int): ArrayAdapter<CharSequence> {
        return ArrayAdapter.createFromResource(
                myContext,
                array_type,
                android.R.layout.simple_spinner_dropdown_item
        )
    }

    private fun createSpinner(rootView: View, adapter: ArrayAdapter<CharSequence>, spinnerId: Int): Spinner {
        val spinner = rootView.findViewById<Spinner>(spinnerId)
        spinner.adapter = adapter
        return spinner
    }

    inner class CreateTaskResponder: ResultReceiver(null) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
            if (resultCode == OwoCalendarService.OK_RESULT) {
                val action = resultData.getString(OwoCalendarService.ACTION)
                if (action == OwoCalendarService.ADD_EVENT) {
                    Log.e("CreateTaskResponder", "added event :D")
                    Toast.makeText(requireContext(), "Tarea agregada", Toast.LENGTH_LONG).show()
                    owoEventManager?.onEventAdded()
                }
            } else {
                val errorString = resultData.getString(OwoCalendarService.ERROR_MESSAGE)
                Log.e("CreateTaskResponder", errorString)
            }
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        val parentFragment = targetFragment
        if (parentFragment is DialogInterface.OnDismissListener) {
            (parentFragment as DialogInterface.OnDismissListener).onDismiss(dialog)
        }
    }

}