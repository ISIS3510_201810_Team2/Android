package com.themelt.owo.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import com.ibm.watson.developer_cloud.conversation.v1.model.Context
import com.themelt.owo.*
import com.themelt.owo.bot.BotDialog
import com.themelt.owo.models.EventModel
import android.widget.Button
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit
import com.themelt.owo.main.MainActivity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.widget.ImageView
import mobi.upod.timedurationpicker.TimeDurationPickerDialog
import java.lang.ref.WeakReference
import java.util.*
import java.util.Calendar


class Feed : MainFragment(), DialogInterface.OnDismissListener, AdapterCallback {

    var owoFab: FloatingActionButton?= null
    private lateinit var viewManager: RecyclerView.LayoutManager

    lateinit var viewAdapter: MyAdapter

    lateinit var recycleView: RecyclerView

    lateinit var textBreaks: TextView

    lateinit var textBreakImage: ImageView

    lateinit var textVacio: TextView

    var listaVacia: Boolean= true

    override val TAG=com.themelt.owo.FEED

    companion object {
        fun newInstance() = Feed()
    }

    private lateinit var viewModel: FeedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.feed_fragment, container, false)
        owoFab= view.findViewById(R.id.owo_fab)

        owoFab?.setOnClickListener {
            openDialog()
            owoFab?.isEnabled= false
        }

        textBreaks= view.findViewById(R.id.textBreaks)
        textBreakImage= view.findViewById(R.id.image_break)
        textVacio= view.findViewById(R.id.textVacio)

        viewManager = LinearLayoutManager(activity!!.applicationContext)

        (viewManager as LinearLayoutManager).orientation = LinearLayoutManager.HORIZONTAL
        (viewManager as LinearLayoutManager).isSmoothScrollbarEnabled = true

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FeedViewModel::class.java)
        appViewModel= activity?.run {
            ViewModelProviders.of(this).get(AppSubClass::class.java)
        } ?: throw Exception("Invalid Activity")

        val cardSleeping= view!!.findViewById<CardView>(R.id.card_sleep)
        val cardWakeUp= view!!.findViewById<CardView>(R.id.card_wake_up)


        val butSleep= cardSleeping.findViewById<FloatingActionButton>(R.id.fab_sleep)
        val butWakeUp= cardWakeUp.findViewById<FloatingActionButton>(R.id.fab_wake_up)


        butSleep.setOnClickListener {
            Log.v("Llega dormir", "buttrue")
           appViewModel.setSleeping(true)
        }

        butWakeUp.setOnClickListener {
            Log.v("Llega dormir", "butfalse")
            appViewModel.setSleeping(false)
        }

        viewAdapter = MyAdapter(ArrayList(),WeakReference(activity!!),this)

        recycleView = view!!.findViewById<RecyclerView>(R.id.recommendationsView).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter


        }
        Log.v("Llega","Recommended se llama")

        appViewModel.getSleepingObserver().observe(this, Observer<Boolean> { sleeping ->
            if(sleeping!!)
            {
                cardSleeping.visibility= View.INVISIBLE
                cardWakeUp.visibility= View.VISIBLE
                butSleep.isEnabled= false
                butWakeUp.isEnabled= true
            }
            else{
                cardSleeping.visibility= View.VISIBLE
                cardWakeUp.visibility= View.INVISIBLE
                butSleep.isEnabled= true
                butWakeUp.isEnabled= false
            }
        })
        Log.v("Drawer", "Llega feed")
    }

    override fun onDismiss(dialog: DialogInterface?) {
        owoFab?.isEnabled= true
    }

    override fun postAction(action: String, wContext: Context) {
        Log.v("Llega accion 2", action)
        if(action.startsWith(FEED)){
            Log.v("Llega accion 2", "Pre Ejecuta")
            Log.v("Llega accion 2", action.split( ":")[1])
        }
        else{
            owoFab?.isEnabled=true
            (activity as MainActivity).action(action, dialog!!,wContext)
        }
    }

    override fun addMessageFromBot(message: String) {
        dialog?.sendBotMessage(message)
    }

    override fun openDialog() {
        dialog = BotDialog()
        val args = Bundle()
        args.putString("initFragment", TAG)
        dialog?.arguments = args

        dialog?.setTargetFragment(this@Feed, 1)
        dialog?.show(fragmentManager, "OwO Dialog")
        this.fragmentManager?.executePendingTransactions()

        val d = dialog?.dialog
        d?.window?.setBackgroundDrawable(null)
    }

    override fun onItemsChanged(size: Int) {
        if(size==0 && !listaVacia){
            textVacio.visibility= View.VISIBLE
            textBreaks.visibility= View.GONE
            textBreakImage.visibility= View.GONE
            recycleView.visibility= View.GONE
            listaVacia=true
        }
        else if(size>0 && listaVacia){
            textVacio.visibility= View.GONE
            textBreaks.visibility= View.VISIBLE
            textBreakImage.visibility= View.VISIBLE
            recycleView.visibility= View.VISIBLE
            listaVacia=false
        }
    }
}

class MyAdapter(private var myDataset: ArrayList<EventModel>, private var context: WeakReference<android.content.Context>, val adapterCallback: AdapterCallback) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class MyViewHolderEvent(v: View) : RecyclerView.ViewHolder(v) {
        // each data item is just a string in this case
        var textTipo: TextView = v.findViewById(R.id.text_tipo)
        var textFecha: TextView = v.findViewById(R.id.text_fecha_evento)
        var textHora: TextView = v.findViewById(R.id.text_hora_evento)
        var butFecha: Button = v.findViewById(R.id.butFecha)
        var butHora: Button = v.findViewById(R.id.butHora)
        var butDuracion: Button = v.findViewById(R.id.butDuracion)

        var butDescartar: FloatingActionButton = v.findViewById(R.id.butDescartar)
        var butAgregar: FloatingActionButton = v.findViewById(R.id.butAgregar)

    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.card_recomendations, parent, false)
        return MyViewHolderEvent(v)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = myDataset[holder.adapterPosition]


        val end = item.endDate!!.timeInMillis
        val start = item.startDate!!.timeInMillis
        val dur= TimeUnit.MILLISECONDS.toMillis(Math.abs(end - start))
        val durMin= dur/(60*1000)

        (holder as MyViewHolderEvent).butFecha.setOnClickListener {
            holder.butFecha.isEnabled=false
            val date=item.startDate
            Log.v("RV recomend","butFecha")
            if(context.get()==null) return@setOnClickListener
            val recogerFecha = DatePickerDialog(context.get()!!, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                item.startDate.set(Calendar.MONTH,month)
                item.startDate.set(Calendar.YEAR,year)
                item.startDate.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                item.endDate.timeInMillis= item.startDate.timeInMillis + dur
                notifyItemChanged(holder.adapterPosition)
            },date.get(Calendar.YEAR),date.get(Calendar.MONTH),date.get(Calendar.DAY_OF_MONTH))
            //Muestro el widget
            recogerFecha.show()
            recogerFecha.setOnDismissListener{
                holder.butFecha.isEnabled=true
            }
        }
        val formatoFecha= SimpleDateFormat("dd 'de' LLLL 'de' yyyy", Locale.getDefault()).format(item.startDate.time)
        val formatoHora= SimpleDateFormat("hh:mm", Locale.getDefault()).format(item.startDate.time)
        holder.butFecha.text= formatoFecha

        holder.butHora.setOnClickListener {
            Log.v("RV recomend","butHora")
        }
        holder.butHora.setOnClickListener {
            holder.butHora.isEnabled=false
            val date=item.startDate
            Log.v("RV recomend","butFecha")
            if(context.get()==null) return@setOnClickListener
            val recogerFecha = TimePickerDialog(context.get()!!, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
                item.startDate.set(Calendar.HOUR_OF_DAY,selectedHour)
                item.startDate.set(Calendar.MINUTE,selectedMinute)
                item.endDate.timeInMillis= item.startDate.timeInMillis + dur
                notifyItemChanged(holder.adapterPosition)
            }, date.get(Calendar.HOUR_OF_DAY),date.get(Calendar.MINUTE), true)//Yes 24 hour time
            //Muestro el widget
            recogerFecha.show()
            recogerFecha.setOnDismissListener{
                holder.butHora.isEnabled=true
            }
        }
        holder.butHora.text= formatoHora

        holder.butDuracion.setOnClickListener {
            holder.butDuracion.isEnabled=false
            Log.v("RV recomend","butDuracion")
            if(context.get()==null) return@setOnClickListener
            val recorgerFecha = TimeDurationPickerDialog(context.get()!!, TimeDurationPickerDialog.OnDurationSetListener{_,time ->
                Log.v("Tim", time.toString())
                item.endDate.timeInMillis= item.startDate.timeInMillis+ time
                notifyItemChanged(holder.adapterPosition)

            },dur )
            recorgerFecha.show()
            recorgerFecha.setOnDismissListener{
                holder.butDuracion.isEnabled=true
            }
        }

        holder.butDescartar.setOnClickListener {
            Log.v("RV recomend","butDescartar")
            if(context.get()==null) return@setOnClickListener
            myDataset.removeAt(holder.adapterPosition)
            notifyItemRemoved(holder.adapterPosition)
            notifyItemRangeChanged(holder.adapterPosition, myDataset.size)
            adapterCallback.onItemsChanged(myDataset.size)
        }

        holder.butAgregar.setOnClickListener {
            Log.v("RV recomend","butAgregar")
            if(context.get()==null) return@setOnClickListener
            (context.get()!! as MainActivity).addEvent(item,null)
            myDataset.removeAt(holder.adapterPosition)
            notifyItemRemoved(holder.adapterPosition)
            notifyItemRangeChanged(holder.adapterPosition, myDataset.size)
            adapterCallback.onItemsChanged(myDataset.size)
        }

        holder.textTipo.text=item.title
        holder.textFecha.text=formatoFecha
        holder.textHora.text=formatoHora



        holder.butDuracion.text= context.get()!!.getString(R.string.duration, durMin/60, durMin%60)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size

    fun setData(events: ArrayList<EventModel>){
        Log.v("lLEGA RECOMMEND","LLEGA RECOMMEND")
        myDataset=events
        Log.v("Recomend", events.size.toString())
        notifyDataSetChanged()
        adapterCallback.onItemsChanged(events.size)
    }
}

interface AdapterCallback {
    fun onItemsChanged(size: Int)
}
