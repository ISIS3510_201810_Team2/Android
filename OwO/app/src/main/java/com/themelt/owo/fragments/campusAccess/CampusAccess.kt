package com.themelt.owo.fragments.campusAccess

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Bitmap
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.themelt.owo.R
import com.themelt.owo.bot.BotDialog
import android.content.Context.MODE_PRIVATE
import android.content.DialogInterface
import java.text.SimpleDateFormat
import java.util.*
import android.text.format.DateUtils
import android.util.Log
import android.widget.ProgressBar
import android.widget.TextView
import com.ibm.watson.developer_cloud.conversation.v1.model.Context
import com.themelt.owo.AppSubClass
import com.themelt.owo.CAMPUS_ACCESS
import com.themelt.owo.MainFragment
import com.themelt.owo.main.MainActivity


class CampusAccess : MainFragment(), DialogInterface.OnDismissListener  {

    var owoFab: FloatingActionButton?= null

    override val TAG=com.themelt.owo.CAMPUS_ACCESS

    val QR_SHARED_PREFERENCES="QrSharedPreferences"

    var pbCampusAccess: ProgressBar?=null

    var textInfo: TextView?=null

    companion object {
        fun newInstance() = CampusAccess()
    }

    private lateinit var viewModel: CampusAccessViewModel

    var imageView: ImageView?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.campus_access_fragment, container, false)
        dialog= BotDialog()
        owoFab= view.findViewById(R.id.owo_fab)

        owoFab?.setOnClickListener {
            openDialog()
            owoFab?.isEnabled= false
        }

        pbCampusAccess= view!!.findViewById(R.id.pbCampusAccess)
        textInfo= view.findViewById(R.id.textQrInfo)


        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        appViewModel= activity?.run {
            ViewModelProviders.of(this).get(AppSubClass::class.java)
        } ?: throw Exception("Invalid Activity")
        viewModel = ViewModelProviders.of(this).get(CampusAccessViewModel::class.java)

        val butAccess= view!!.findViewById<CardView>(R.id.access_request_button)

        butAccess.setOnClickListener {
            openDialog()
            dialog?.sendInteractiveMessage("Acceso a Campus")
            //viewModel.loadData(context,  (activity?.applicationContext as AppSubClass).getInstance()?.getAuthResult()?.accessToken)

        }

        imageView=  view!!.findViewById(R.id.qr_image_view)

        viewModel.getQrCode().observe(this, Observer<String> { qrCode ->
            Log.v("Debe llegar QR", qrCode)
            if (qrCode != null && !qrCode.isEmpty()) {
                QRGenerator(this).execute(qrCode)
                val sdfQR = SimpleDateFormat("dd-MM-yyyy", Locale("es","CO"))
                val editor = activity?.getSharedPreferences(QR_SHARED_PREFERENCES, MODE_PRIVATE)?.edit()
                Log.v("Llega shared", editor.toString())
                editor?.putString("qrCode", qrCode)
                editor?.putString("qrDate", sdfQR.format(Date()))
                editor?.apply()
            }
            else{
                pbCampusAccess?.visibility= View.INVISIBLE
                textInfo?.visibility= View.VISIBLE
            }
        })

        viewModel.getMessageFromBot().observe(this, Observer<String> { message ->
            if (message != null && !message.isEmpty()) {
                addMessageFromBot(message)
            }
        })

        val prefs = activity?.getSharedPreferences(QR_SHARED_PREFERENCES, MODE_PRIVATE)
        Log.v("Shared QR acceso", prefs.toString())
        val qrCDate = prefs?.getString("qrDate", null)
        Log.v("Shared QR date", qrCDate.toString())
        if (qrCDate != null) {
            Log.v("Llega shared date",qrCDate.toString())
            val sdfQR = SimpleDateFormat("dd-MM-yyyy", Locale("es","CO"))
            val cal= Calendar.getInstance()
            cal.time = sdfQR.parse(qrCDate)
            if(DateUtils.isToday(sdfQR.parse(qrCDate).time)){
                Log.v("Llega shared date pro","entra")
                viewModel.getQrCode().value=(prefs.getString("qrCode", null))
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        owoFab?.isEnabled= true
    }

    fun updateQRImage(result: Bitmap?) {
        pbCampusAccess?.visibility= View.INVISIBLE
        imageView?.setImageBitmap(result)
    }

    override fun postAction(action: String, wContext: Context) {
        Log.v("Llega accion 2", action)
        if(action.startsWith(CAMPUS_ACCESS)){
            Log.v("Llega accion 2", "Pre Ejecuta")
            Log.v("Llega accion 2", action.split( ":")[1])
            when(action.split(":")[1]){
                "obtenerQR"->{
                    Log.v("Llega accion 2", "Ejecuta")
                    pbCampusAccess?.visibility= View.VISIBLE
                    textInfo?.visibility= View.INVISIBLE
                    viewModel.loadData(context,  appViewModel.getAuthResult()?.accessToken)
                }
            }
        }
        else{
            owoFab?.isEnabled=true
            (activity as MainActivity).action(action, dialog!!,wContext)
        }
    }

    override fun addMessageFromBot(message: String) {
        dialog?.sendBotMessage(message)
    }

    override fun openDialog() {

        val args = Bundle()
        args.putString("initFragment", TAG)
        dialog?.arguments = args

        dialog?.setTargetFragment(this@CampusAccess, 1)
        dialog?.show(fragmentManager, "OwO Dialog")
        this.fragmentManager?.executePendingTransactions()

        val d = dialog?.dialog
        d?.window?.setBackgroundDrawable(null)
    }


}
