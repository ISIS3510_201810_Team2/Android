package com.themelt.owo

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.content.Intent
import com.microsoft.identity.client.*
import com.themelt.owo.main.MainActivity
import com.microsoft.identity.client.PublicClientApplication




class Splash : AppCompatActivity() {

    val CLIENT_ID = "5301a170-53af-4a3f-a70a-a06a1dc9ff04"
    val SCOPES = arrayOf("https://graph.microsoft.com/User.Read")

    val TAG = "Splash activity"

    lateinit var appViewModel:AppSubClass

    /* Azure AD Variables */



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        /* Configure your sample app and save state for this activity */

        appViewModel = ViewModelProviders.of(this).get(AppSubClass::class.java)
        var sampleApp = appViewModel.sampleApp

        /* Initialize the MSAL App context */
        if (sampleApp == null) {
            sampleApp = PublicClientApplication(
                    this.applicationContext,
                    CLIENT_ID)
            appViewModel.sampleApp= sampleApp
        }

        /* Attempt to get a user and acquireTokenSilent
        * If this fails we do an interactive request
        */
            val users = sampleApp.users

            if (users != null && users.size == 1) {
                /* We have 1 user */

                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                /* We have no user */
                /* Let's do an interactive request */
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }

    }
}
