package com.themelt.owo.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import com.themelt.owo.R
import android.widget.RemoteViews
import android.content.BroadcastReceiver
import android.support.v4.app.TaskStackBuilder
import com.themelt.owo.main.MainActivity


class NotificationHelper(private val mContext: Context) {
    private var mNotificationManager: NotificationManager? = null
    private var mBuilder: NotificationCompat.Builder? = null

    val NOTIFICATION_CHANNEL_ID = "10001"

    /**
     * Create and push the notification
     */
    fun createNotification(title: String, message: String) {
        /**Creates an explicit intent for an Activity in your app */
        val resultIntent = Intent(mContext, MainActivity::class.java)
        resultIntent.putExtra("SleepNotification", false)

        val resultPendingIntent = PendingIntent.getActivity(mContext,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        mBuilder = NotificationCompat.Builder(mContext,NOTIFICATION_CHANNEL_ID)



        mNotificationManager = mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        val contentView = RemoteViews(mContext.packageName, R.layout.notification_sleep)
        val contentViewExpanded = RemoteViews(mContext.packageName, R.layout.notification_sleep_expanded)


        mBuilder!!.setSmallIcon(android.R.color.transparent)
                .setAutoCancel(false)
                .setOngoing(true)
                .setCustomContentView(contentView)
                .setCustomBigContentView(contentViewExpanded)
                .setContentIntent(resultPendingIntent)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_LOW
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            mBuilder!!.setChannelId(NOTIFICATION_CHANNEL_ID)
            mNotificationManager!!.createNotificationChannel(notificationChannel)
        }



        val sleepIntent = Intent(mContext, MainActivity::class.java)
        sleepIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK  or Intent.FLAG_ACTIVITY_CLEAR_TASK
        sleepIntent.putExtra("SleepNotification", true)
        val stackBuilder = TaskStackBuilder.create(mContext)
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity::class.java)
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(sleepIntent)
        val sleepPendingIntent = stackBuilder.getPendingIntent(
                1,
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        contentViewExpanded.setOnClickPendingIntent(R.id.button_notification,
                sleepPendingIntent)

        mNotificationManager!!.notify(0 /* Request Code */, mBuilder!!.build())
    }

    fun cancelNotification(){
        mNotificationManager = mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager!!.cancel(0)
    }

}