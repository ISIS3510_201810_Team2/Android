package com.themelt.owo.services

import android.Manifest
import android.app.IntentService
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.os.Bundle
import android.os.ResultReceiver
import android.provider.CalendarContract
import android.support.v4.content.ContextCompat
import android.util.Log
import com.themelt.owo.models.EventModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class OwoCalendarService : IntentService("OwoCalendarService") {

    companion object {
        val ACTION = "action"
        val RESULT_RECEIVER = "result_receiver"
        val ADD_EVENT = "add_event"
        val GET_RECOMMENDED = "get_recomended"
        val EDIT_EVENT = "edit_event"
        val EVENT = "event"
        val RECOMMENDED_RESULT = "recommended_result"
        val CALENDAR_NAME = "Calendario OwO"
        val EXCHANGE_ACCOUNT_TYPE = "com.google.android.gm.exchange"
        val ACCOUNT_NAME_PARAM = "account_name"
        val OK_RESULT = 0
        val ERROR_RESULT = 1
        val ERROR_MESSAGE = "error_message"
        val PERMISSION_ERROR = "permission_error"
        val MORNING_MIN = 7
        val EVENING_MAX = 22
    }

    private val TAG = "OwoCalendarService"

    private var accountName: String? = null

    override fun onHandleIntent(intent: Intent?) {
        // Intent MUST have action parameter and account name
        Log.i(TAG, "Intent Service started")
        val action = intent?.getStringExtra(ACTION)
        val resultReceiver = intent?.getParcelableExtra<ResultReceiver>(RESULT_RECEIVER)
        accountName = intent?.getStringExtra(ACCOUNT_NAME_PARAM)
        // Here, thisActivity is the current activity
        val resultData = Bundle()
        resultData.putString(ACTION, action)
        if (ContextCompat.checkSelfPermission(applicationContext,
                        Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(applicationContext,
                        Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            resultData.putString(ERROR_MESSAGE, PERMISSION_ERROR)
            resultReceiver?.send(ERROR_RESULT, resultData)
        } else if (action.equals(ADD_EVENT)) {
            val event = intent?.getParcelableExtra<EventModel>(EVENT)
            if (event != null) {
                event.calendarId = getCalendarId()
                event.id = addEvent(event)
                resultData.putParcelable(EVENT, event)
                resultReceiver?.send(OK_RESULT, resultData)
            } else {
                resultData.putString(ERROR_MESSAGE, "EventModel is null")
                resultReceiver?.send(ERROR_RESULT, resultData)
            }
        } else if (action.equals(EDIT_EVENT)) {
            val event = intent?.getParcelableExtra<EventModel>(EVENT)
            if (event != null && event.id != -1L) {
                event.calendarId = getCalendarId()
                TODO("be able to update events")
                //updateEvent(event)
            } else {
                resultData.putString(ERROR_MESSAGE, if (event == null) "EventModel is null" else "EventModel doesn't have id")
                resultReceiver?.send(ERROR_RESULT, resultData)
            }
        } else if (action.equals(GET_RECOMMENDED)) {
            val events = getRecommendedEvents()
            resultData.putParcelableArrayList(RECOMMENDED_RESULT, events)
            resultReceiver?.send(OK_RESULT, resultData)
        }
    }

    private fun getEvents(selection: String, selectionArgs: Array<String>, sortOrder: String): ArrayList<EventModel> {
        val events = arrayListOf<EventModel>()
        var cur: Cursor? = null
        val cr = contentResolver

        val mProjection = arrayOf(
                CalendarContract.Events._ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DESCRIPTION,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND)
        val id_index = 0
        val title_index = 1
        val description_index = 2
        val calendar_id_index = 3
        val dstart_index = 4
        val dend_index = 5
        val uri = CalendarContract.Events.CONTENT_URI
        cur = cr?.query(uri, mProjection,
                selection, selectionArgs,
                sortOrder)
        var id: Long = 0
        var title: String = ""
        var description = ""
        var calendarId: Long = 0
        var startDate = java.util.Calendar.getInstance()
        var endDate = java.util.Calendar.getInstance()
        while (cur != null && cur.moveToNext()) {
            id = cur.getLong(id_index)
            title = cur.getString(title_index)
            description = cur.getString(description_index) ?: ""
            calendarId= cur.getLong(calendar_id_index)
            startDate.timeInMillis = cur.getLong(dstart_index)
            endDate.timeInMillis = cur.getLong(dend_index)
            events.add(EventModel(id, title, description, calendarId, startDate, endDate))
            startDate = Calendar.getInstance()
            endDate = Calendar.getInstance()
        }
        return events
    }

    private fun fixToWakeHours(lastFreeTime: Calendar) {
        if (lastFreeTime.get(Calendar.HOUR_OF_DAY) < MORNING_MIN) {
            lastFreeTime.set(Calendar.HOUR_OF_DAY, MORNING_MIN)
            lastFreeTime.set(Calendar.MINUTE, 0)
        } else if (lastFreeTime.get(Calendar.HOUR_OF_DAY) >= EVENING_MAX) {
            lastFreeTime.add(Calendar.DATE, 1)
            lastFreeTime.set(Calendar.HOUR_OF_DAY, MORNING_MIN)
            lastFreeTime.set(Calendar.MINUTE, 0)
        }
    }

    private fun getFreeTime(): ArrayList<EventModel> {
        val today = java.util.Calendar.getInstance()
        val fiveDays = java.util.Calendar.getInstance()
        fiveDays.add(java.util.Calendar.DATE, 5)
        val selection = "(${CalendarContract.Events.DTEND} > ?) AND (${CalendarContract.Events.DTSTART} <= ?)"
        val selectionArgs = arrayOf(today.timeInMillis.toString(), fiveDays.timeInMillis.toString())
        val sortOrder = "${CalendarContract.Events.DTSTART} ASC"
        val busyEvents = getEvents(selection, selectionArgs, sortOrder)

        var lastFreeTime = java.util.Calendar.getInstance()
        val events = arrayListOf<EventModel>()
        var calStart = java.util.Calendar.getInstance()
        var calEnd = java.util.Calendar.getInstance()
        val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm")
        var secondsDiff = 0L
        var startD = java.util.Calendar.getInstance()
        var hoursDiff = 0L
        var minutesDiff = 0L
        for (event in busyEvents) {
            fixToWakeHours(lastFreeTime)
            startD = event.startDate!!
            secondsDiff = (startD.timeInMillis - lastFreeTime.timeInMillis) / 1000
            hoursDiff = (secondsDiff / 3600)
            minutesDiff = 0L
            if (hoursDiff >= 1) {
                while (compareCalendarNoTime(lastFreeTime, startD) < 0) {
                    calStart.timeInMillis = lastFreeTime.timeInMillis
                    calEnd.timeInMillis = calStart.timeInMillis
                    calEnd.set(Calendar.HOUR_OF_DAY, EVENING_MAX)
                    secondsDiff = (calEnd.timeInMillis - calStart.timeInMillis) / 1000
                    minutesDiff = secondsDiff / 60
                    if (minutesDiff >= 30) {
                        events.add(EventModel(startDate = calStart, endDate = calEnd))
                    }
                    lastFreeTime.timeInMillis = calEnd.timeInMillis
                    lastFreeTime.add(Calendar.HOUR, 1)
                    fixToWakeHours(lastFreeTime)
                    calStart = Calendar.getInstance()
                    calEnd = Calendar.getInstance()
                }
                if (compareCalendarNoTime(lastFreeTime, startD) == 0) {
                    calStart.timeInMillis = lastFreeTime.timeInMillis
                    calEnd.timeInMillis = startD.timeInMillis
                    secondsDiff = (calEnd.timeInMillis - calStart.timeInMillis) / 1000
                    minutesDiff = secondsDiff / 60
                    if (minutesDiff >= 30) {
                        events.add(EventModel(startDate = calStart, endDate = calEnd))
                    }
                    if (event.endDate!!.timeInMillis > lastFreeTime.timeInMillis) {
                        lastFreeTime.timeInMillis = event.endDate!!.timeInMillis
                    }
                    calStart = Calendar.getInstance()
                    calEnd = Calendar.getInstance()
                }
            } else {
                if (event.endDate!!.timeInMillis > lastFreeTime.timeInMillis) {
                    lastFreeTime.timeInMillis = event.endDate!!.timeInMillis
                }
            }
        }
        return events
    }

    private fun getOwoEvents(): ArrayList<EventModel> {
        val calendarId = getCalendarId()
        val selection = "(${CalendarContract.Events.DTEND} >= ?) AND " +
                "(${CalendarContract.Events.CALENDAR_ID} = ?) AND " +
                "(${CalendarContract.Events.DESCRIPTION} = 'Parcial' " +
                "OR ${CalendarContract.Events.DESCRIPTION} = 'Tarea')"
        val selectionArgs = arrayOf(
                java.util.Calendar.getInstance().timeInMillis.toString(),
                calendarId.toString())
        val sortOrder = "${CalendarContract.Events.DTSTART} ASC LIMIT 5"
        return getEvents(selection, selectionArgs, sortOrder)
    }

    private fun getRecommendedEvents(): ArrayList<EventModel> {
        val freeTimeEvents = getFreeTime()
        val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val owoEvents = getOwoEvents()
        val events = arrayListOf<EventModel>()
        val busyFreeTimes = mutableSetOf<String>()
        for (owoEvent in owoEvents) {
            for (freeTime in freeTimeEvents) {
                if (owoEvent.startDate!!.timeInMillis >= freeTime.endDate!!.timeInMillis &&
                        !busyFreeTimes.contains(freeTime.toString())) {
                    busyFreeTimes.add(freeTime.toString())
                    events.add(
                            EventModel(
                                    title = "Estudiar para ${owoEvent.title}",
                                    description = "Estudiar",
                                    startDate = freeTime.startDate,
                                    endDate = freeTime.endDate))
                    break
                }
            }
        }
        Log.i(TAG, "done get recommended events")
        for (e in events) {
            Log.i(TAG, "Recommend ${e.title}")
            Log.i(TAG, "From: ${dateFormat.format(e.startDate!!.time)}, To: ${dateFormat.format(e.endDate!!.time)}")
        }
        return events
    }

    private fun getCalendarId(): Long {
        var cur: Cursor? = null
        val cr = contentResolver

        val mProjection = arrayOf(CalendarContract.Calendars._ID)

        val uri = CalendarContract.Calendars.CONTENT_URI;
        val selection = """((${CalendarContract.Calendars.ACCOUNT_NAME} = ?) AND
                                  |(${CalendarContract.Calendars.ACCOUNT_TYPE} = ?) AND
                                  |(${CalendarContract.Calendars.NAME} = ?))""".trimMargin()
        val selectionArgs = arrayOf(accountName, EXCHANGE_ACCOUNT_TYPE, CALENDAR_NAME)

        // TODO("MOVE THIS SOMEWHERE")
        //if (ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
        //  ActivityCompat.requestPermissions(ac, arrayOf(Manifest.permission.READ_CALENDAR), targetRequestCode)
        //}
        cur = cr.query(uri, mProjection, selection, selectionArgs, null);

        if (cur.moveToFirst()) {
            return cur.getLong(0)
        } else {
            return createCalendar()
        }
    }

    private fun createCalendar(): Long {
        val x = ArrayList<String>()
        val contentValues = ContentValues()
        contentValues.put(CalendarContract.Calendars.ACCOUNT_NAME, accountName)
        contentValues.put(CalendarContract.Calendars.ACCOUNT_TYPE, EXCHANGE_ACCOUNT_TYPE)
        contentValues.put(CalendarContract.Calendars.NAME, CALENDAR_NAME)
        contentValues.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, CALENDAR_NAME)
        contentValues.put(CalendarContract.Calendars.CALENDAR_COLOR, Color.YELLOW)
        contentValues.put(CalendarContract.Calendars.VISIBLE, 1)
        contentValues.put(
                CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL,
                CalendarContract.Calendars.CAL_ACCESS_OWNER)
        contentValues.put(CalendarContract.Calendars.OWNER_ACCOUNT, accountName)
        contentValues.put(
                CalendarContract.Calendars.ALLOWED_REMINDERS,
                "METHOD_ALERT, METHOD_EMAIL, METHOD_ALARM")
        contentValues.put(
                CalendarContract.Calendars.ALLOWED_ATTENDEE_TYPES,
                "TYPE_OPTIONAL, TYPE_REQUIRED, TYPE_RESOURCE")
        contentValues.put(
                CalendarContract.Calendars.ALLOWED_AVAILABILITY,
                "AVAILABILITY_BUSY, AVAILABILITY_FREE, AVAILABILITY_TENTATIVE")

        var uri = CalendarContract.Calendars.CONTENT_URI
        uri = uri.buildUpon()
                .appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, accountName)
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, EXCHANGE_ACCOUNT_TYPE).build()
        val ansUri = contentResolver.insert(uri, contentValues)
        return ansUri.lastPathSegment.toLong()
    }

    private fun addEvent(event: EventModel): Long {
        val cr = contentResolver
        val defaultTime = java.util.Calendar.getInstance().timeInMillis
        val values = ContentValues()
        values.put(CalendarContract.Events.TITLE, event.title)
        values.put(CalendarContract.Events.DESCRIPTION, event.description)
        values.put(CalendarContract.Events.CALENDAR_ID, event.calendarId)
        values.put(CalendarContract.Events.DTSTART, event.startDate?.timeInMillis ?: defaultTime)
        values.put(CalendarContract.Events.DTEND, event.endDate?.timeInMillis ?: defaultTime)
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().toString())

        val uri = cr.insert(CalendarContract.Events.CONTENT_URI, values)
        return uri.lastPathSegment.toLong()
    }

    private fun compareCalendarNoTime(c1: Calendar, c2: Calendar): Int {
        if (c1.get(Calendar.YEAR) !== c2.get(Calendar.YEAR))
            return c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR)
        return if (c1.get(Calendar.MONTH) !== c2.get(Calendar.MONTH)) c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH) else c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH)
    }
}
