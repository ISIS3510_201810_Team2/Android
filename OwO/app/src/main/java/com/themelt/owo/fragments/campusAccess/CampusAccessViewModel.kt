package com.themelt.owo.fragments.campusAccess

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject
import com.android.volley.AuthFailureError
import com.google.zxing.qrcode.encoder.QRCode
import com.themelt.owo.utils.RequestQueueSingleton


class CampusAccessViewModel : ViewModel() {
    val TAG= "CampusAccess view model"



    private lateinit var qrCode: MutableLiveData<String>

    private lateinit var messageFromBot: MutableLiveData<String>



     fun loadData(context: Context?, token: String?) {
         if(token==null)
         {
             Toast.makeText(context, "No se pudo obtener el QR :c, revisa tu conexión a internet", Toast.LENGTH_LONG).show()
             return
         }
         else{
             Log.v("El token",token)
         }
        val requestQueue = RequestQueueSingleton.getInstance(context!!)
        val url = "https://dev-api.agentowl.me/uniandes/p2000/access?type=qr"
         val parameters = JSONObject()
         try {
             parameters.put("key", "value")
         } catch (e: Exception) {
         }

         val req: JsonObjectRequest = object : JsonObjectRequest(Request.Method.POST, url, parameters,
                Response.Listener<JSONObject> { response ->
                    Log.v("a br 2", response.getJSONObject("data").getString("code"))
                    getQrCode().value= response.getJSONObject("data").getString("code")

                },
                Response.ErrorListener { error -> Log.d(TAG, error.toString())
                Log.d("Sniffiwi", "Error de conexion")
                    messageFromBot.postValue("Ha ocurrido un error con los servidores de la Universidad para esta función, intenta más tarde")
                }
        ) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //headers.put("Content-Type", "application/json");
                headers["Authorization"] = "Bearer $token"
                headers["Content-Type"] = "application/json"
                return headers
            }
        }
         requestQueue.addToRequestQueue(req)
    }

    fun getQrCode(): MutableLiveData<String> {
        if (!::qrCode.isInitialized) {
            qrCode = MutableLiveData()
            qrCode.value=""
        }
        return qrCode
    }

    fun getMessageFromBot(): MutableLiveData<String> {
        if (!::messageFromBot.isInitialized) {
            messageFromBot = MutableLiveData()
            messageFromBot.value=""
        }
        return messageFromBot
    }
}
