package com.themelt.owo.bot

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import android.util.Log
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageOptions
import com.ibm.watson.developer_cloud.conversation.v1.model.InputData
import com.ibm.watson.developer_cloud.conversation.v1.Conversation
import com.ibm.watson.developer_cloud.conversation.v1.model.Context

class BotDialogViewModel : ViewModel()

{

    // Create a LiveData with a String
    private lateinit var messages:  MutableLiveData<MutableList<BotMessage>>
    private lateinit var handler:  BotMessageHandler
    private lateinit var watsonUsername: String
    private lateinit var watsonPassword: String
    private lateinit var watsonWorkspaceId: String
    private lateinit var accion: MutableLiveData<String>
    private lateinit var service: Conversation
    private var sleeping: Boolean= false
    var context: com.ibm.watson.developer_cloud.conversation.v1.model.Context = Context()

    fun getMessages(): MutableLiveData<MutableList<BotMessage>> {
        if (!::messages.isInitialized) {
            messages = MutableLiveData()
            messages.value= mutableListOf()
        }
        return messages
    }

    fun getAccion(): LiveData<String>{
        if (!::accion.isInitialized) {
            accion = MutableLiveData()
            accion.value= ""
        }
        return accion
    }

    fun getHandler():BotMessageHandler{
        if (!::handler.isInitialized) {
            handler= BotMessageHandler()
        }
        return handler
    }

    fun getService():Conversation{
        if (!::service.isInitialized) {
            service = Conversation(Conversation.VERSION_DATE_2017_05_26,watsonUsername, watsonPassword)
            service.endPoint = "https://gateway.watsonplatform.net/assistant/api"
            context["timezone"]="America/Bogota"
            context["sleeping"]=sleeping.toString()
            context["chacharaCounter"]=0
            Log.v("Servicio","inicializacion")
        }
        return service
    }

    fun deleteMessages(){
        getMessages().value= mutableListOf()
    }

    fun addUserMessage(message: String){
        val tempList= getMessages().value
        tempList!!.add(BotMessage(message, false))
        tempList.add(BotMessage("",true).setLoading())
        getMessages().value=tempList
        sendMessage(message,getMessages().value!!)
    }

    fun addBotMessage(message: String){
        val tempList= getMessages().value
        tempList!!.add(BotMessage(message, true))
        getMessages().value=tempList
    }

    fun setWatsonUsername(pWatsonUsername: String) {
        this.watsonUsername = pWatsonUsername
    }

    fun setWatsonPassword(pWatsonPassword: String) {
        this.watsonPassword = pWatsonPassword
    }

    fun setWatsonWorkspaceId(pWatsonWorkspaceId: String) {
        this.watsonWorkspaceId = pWatsonWorkspaceId
    }

    private fun sendMessage(inputMessage: String, curMessages: MutableList<BotMessage>) {
        val thread = Thread(Runnable {
            try {
                val input = InputData.Builder(inputMessage).build()
                val options = MessageOptions.Builder(watsonWorkspaceId).input(input).context(context).build()
                Log.v("Servicio","envio")
                val response = getService().message(options).execute()
                Log.v("Context prev",context.toString())

                //Passing Context of last conversation
                if (response?.context != null) {
                    //context.clear();
                    context = response.context
                    if(!context.containsKey("timezone")){
                        context["timezone"]="America/Bogota"
                    }
                    if(!context.containsKey("sleeping")){
                        context["sleeping"]=sleeping.toString()
                    }
                    if(!context.containsKey("chacharaCounter")){
                        context["chacharaCounter"]=0
                    }
                }
                if (response != null) {
                    if (response.output != null && response.output.containsKey("text")) {
                        val responseList = response.output["text"] as ArrayList<String>?
                        if (null != responseList && responseList.size > 0) {
                            curMessages.removeAt(curMessages.size-1)
                            curMessages.add(BotMessage(responseList[0],true))
                            val fullMessages = if(curMessages.size>20) curMessages.subList(curMessages.size- 20, curMessages.size) else curMessages
                            messages.postValue(fullMessages)
                        }
                    }
                    if(response.output!= null && response.output.containsKey("accion")){
                        val responseAccion = response.output["accion"]  as String
                        accion.postValue(responseAccion)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.v("sniff ", e.message)
                curMessages.removeAt(curMessages.size-1)
                curMessages.add(BotMessage("Lo siento, parece que ahorita no tienes internet :c. Intentalo de nuevo más tarde OwO",true))
            }
        })
        thread.start()

    }

    fun setSleeping(sleeping: Boolean?) {
        this.sleeping=sleeping!!
    }
}
