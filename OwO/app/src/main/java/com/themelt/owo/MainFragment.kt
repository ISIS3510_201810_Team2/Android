package com.themelt.owo

import android.support.v4.app.Fragment
import com.ibm.watson.developer_cloud.conversation.v1.model.Context
import com.themelt.owo.bot.BotDialog

abstract class MainFragment: Fragment(){


    var dialog: BotDialog?= null
    abstract fun postAction(action: String, wContext: Context)

    abstract fun addMessageFromBot(message:String)

    open val TAG="MainFragment"

    lateinit var appViewModel:AppSubClass

    abstract fun openDialog()

}