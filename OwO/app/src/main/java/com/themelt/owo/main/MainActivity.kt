package com.themelt.owo.main

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.ResultReceiver
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.CardView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.TextView
import com.microsoft.identity.client.AuthenticationCallback
import com.microsoft.identity.client.AuthenticationResult
import com.microsoft.identity.client.MsalException
import com.microsoft.identity.client.PublicClientApplication
import com.themelt.owo.fragments.Calendar
import com.themelt.owo.fragments.campusAccess.CampusAccess
import com.themelt.owo.fragments.Feed
import com.microsoft.identity.client.MsalClientException
import android.widget.Toast

import com.themelt.owo.*
import com.themelt.owo.bot.BotDialog
import com.themelt.owo.models.EventModel
import com.themelt.owo.services.OwoCalendarService
import com.themelt.owo.utils.NotificationHelper
import com.themelt.owo.utils.OwoEventManager
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity(), NetworkChangeReceiver.ConectionListener, OwoEventManager {

    companion object {
        val SP_EMAIL_KEY = "user_email"
        val SP_NAME_KEY = "user_name"
    }

    val CURR_TAB = "curr_tab"
    //Fragments

    val feedFragment: Feed = Feed()
    val calendarFragment: Calendar = Calendar()
    val campusFragment: CampusAccess = CampusAccess()

    val fm = supportFragmentManager
    var active: MainFragment = feedFragment

    val TAG = "Main activity"

    var drawerLayout: DrawerLayout? = null
    var navView: NavigationView?= null

    val CLIENT_ID = "5301a170-53af-4a3f-a70a-a06a1dc9ff04"
    val SCOPES = arrayOf("https://graph.microsoft.com/User.Read")

    val SLEEPING_SHARED_PREFERENCES= "SleepingSharedPreferences"

    var conectionIndicator: CardView?=null

    var textConection: TextView?=null

    lateinit var appViewModel:AppSubClass

    var primeraConexion= false
    private var manager: ConnectivityManager? = null

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            // this ternary operation is not quite true, because non-metered doesn't yet mean, that it's wifi
            // nevertheless, for simplicity let's assume that's true
            runOnUiThread {
                onConectionChange(true)
            }
            val delayrunnable = Runnable {
                Log.v("se", "ejecuta")
                Log.v("sea", conectionIndicator.toString() )
                Log.v("sea", conectionIndicator?.layoutParams.toString())
                conectionIndicator?.visibility= View.GONE
            }
            conectionIndicator?.postDelayed(delayrunnable, 3000)
            Log.v("Al menos", "sniff")

        }

        override fun onLost(network: Network) {
            Log.v("Conexion", "llega mal")
            super.onLost(network)
            val connectivityManager = applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            val networkInfo = connectivityManager?.activeNetworkInfo
            if (networkInfo == null || !networkInfo.isConnected) {
                runOnUiThread {
                    onConectionChange(false)
                }
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        appViewModel= ViewModelProviders.of(this).get(AppSubClass::class.java)
        val prefs = getSharedPreferences(SLEEPING_SHARED_PREFERENCES, Context.MODE_PRIVATE)
        appViewModel.setSleeping(prefs.getBoolean("sleeping", false))
        appViewModel.getSleepingObserver().observe(this, Observer<Boolean> { sleeping ->
            if (sleeping!!) {
                NotificationHelper(applicationContext).createNotification("Durmiendo", "Avisame cuando despiertes")
            }
            else{
                NotificationHelper(applicationContext).cancelNotification()
            }
            val editor = prefs.edit()
            editor.putBoolean("sleeping", sleeping)
            editor.apply()
        })

        drawerLayout = findViewById(R.id.main_drawer_layout)
        navView= findViewById(R.id.navigation)
        conectionIndicator= findViewById(R.id.conection_indicator)
        textConection= conectionIndicator?.findViewById(R.id.textConnection)
        configureNavigationDrawer(drawerLayout!!)

        //Initializing viewPager


        //Initializing the bottomNavigationView
        val bottomNavigationView: BottomNavigationView = findViewById<View>(R.id.bottom_navigation) as BottomNavigationView

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener())

        for (fragment in fm.fragments) {
            fm.beginTransaction().remove(fragment).commit()
        }

        fm.beginTransaction().add(R.id.main_container, campusFragment, "3").hide(campusFragment).commit()
        fm.beginTransaction().add(R.id.main_container, calendarFragment, "2").hide(calendarFragment).commit()
        fm.beginTransaction().add(R.id.main_container,feedFragment, "1").hide(feedFragment).commit()

        val selectedTab = savedInstanceState?.getInt(CURR_TAB) ?: 0
        selectFragment(navView?.menu?.getItem(selectedTab)!!)

        val connectivityManager = applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val networkInfo = connectivityManager?.activeNetworkInfo
        if (networkInfo == null || !networkInfo.isConnected) {
            onConectionChange(false)
        }

        if(appViewModel.getAuthResult()==null) {
            setupUser()
        }

        manager =  getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        manager?.registerNetworkCallback(NetworkRequest.Builder().build(),networkCallback)

        showUserData()
        getRecommendedEvents{ events -> feedFragment.viewAdapter.setData(events)  }
    }

    public override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    override fun onResume() {
        super.onResume()
        Log.v("Llega","Llega resume")
        val wokeUp = intent.getBooleanExtra("SleepNotification", false)
        if (wokeUp) {
            Log.v("Ahora sí mamu", "notis")
            intent.removeExtra("SleepNotification")
            NotificationHelper(applicationContext).cancelNotification()
            appViewModel.setSleeping(false)
        }
    }

    override fun onConectionChange(isOnline: Boolean) {
        if (isOnline) {
            Log.v("Conexion","hay")
            conectionIndicator?.layoutParams?.height=50
            conectionIndicator?.visibility= View.VISIBLE
            conectionIndicator?.setBackgroundColor(Color.GREEN)
            textConection?.text = getString(R.string.internet_connected)

        } else {
            Log.v("Conexion","noHay")
            conectionIndicator?.layoutParams?.height=50
            conectionIndicator?.visibility= View.VISIBLE
            conectionIndicator?.setBackgroundColor(Color.RED)
            textConection?.text = getString(R.string.internet_not_connected)

        }
    }

    private fun mOnNavigationItemSelectedListener() = BottomNavigationView.OnNavigationItemSelectedListener{
        return@OnNavigationItemSelectedListener selectFragment(it)

    }

    private fun selectFragment(menuItem: MenuItem): Boolean {
        drawerLayout?.closeDrawer(GravityCompat.START)
        bottom_navigation.menu.findItem(menuItem.itemId).isChecked = true
        navView?.setCheckedItem(menuItem.itemId)

        when (menuItem.itemId) {
            R.id.navigation_feed -> {
                fm.beginTransaction().hide(active).show(feedFragment).commit()
                active = feedFragment
                supportActionBar?.title="Feed"
                //getRecommendedEvents{ events -> (active as Feed).viewAdapter.setData(events)  }
                return true //break
            }
            R.id.navigation_calendar -> {
                fm.beginTransaction().hide(active).show(calendarFragment).commit()
                active = calendarFragment
                supportActionBar?.title="Calendario"
                return true //break
            }
            R.id.navigation_campus_access -> {
                fm.beginTransaction().hide(active).show(campusFragment).commit()
                active = campusFragment
                supportActionBar?.title="Acceso a campus"
                return true //break
            }
            else -> {
                Log.v("Drawer", "Yaper")
                return false //break
            }
        }
    }

    private fun configureNavigationDrawer(drawerLayout: DrawerLayout) {

        val toolbar = findViewById<View>(R.id.main_toolbar) as Toolbar
        setSupportActionBar(toolbar)

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)

        val actionBarDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name)

        drawerLayout.addDrawerListener(actionBarDrawerToggle)

        val textLogout = findViewById<TextView>(R.id.logout)


        navView?.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener { menuItem ->
            return@OnNavigationItemSelectedListener selectFragment(menuItem)
        })


        textLogout.setOnClickListener {
            Log.v("Hol", "Llega logout")
            onSignOutClicked()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        Log.v("Drawer", "Llego item con $itemId")
        drawerLayout?.openDrawer(GravityCompat.START)
        return true
    }

    private fun showUserData() {

        appViewModel.getAuthObserver().observe(this, Observer { auth: AuthenticationResult? ->
            Log.v("Auth", (auth!=null).toString())
            if((auth!=null)){
                val headerLayout = findViewById<NavigationView>(R.id.navigation).getHeaderView(0)
                val txtNombre= headerLayout.findViewById<TextView>(R.id.textNombreDrawer)
                val txtCorreo= headerLayout.findViewById<TextView>(R.id.textCorreoDrawer)
                val nombreValue = appViewModel.getAuthResult()!!.user.name
                val correoValue = appViewModel.getAuthResult()!!.user.displayableId
                txtNombre.text = nombreValue
                txtCorreo.text = correoValue

                //Log.v("a", state?.getAuthResult()!!.tenantId)
                Log.v("a", appViewModel.getAuthResult()!!.idToken)
                Log.v("a", appViewModel.getAuthResult()!!.accessToken)
                //Log.v("a", state?.getAuthResult()!!.uniqueId)

                val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
                with (sharedPref.edit()) {
                    putString(SP_NAME_KEY, nombreValue)
                    putString(SP_EMAIL_KEY, correoValue)
                    apply()
                }
            }
        })

    }

    private fun setupUser(){

        var sampleApp = appViewModel.sampleApp

        /* Initialize the MSAL App context */
        if (sampleApp == null) {
            sampleApp = PublicClientApplication(
                    this.applicationContext,
                    CLIENT_ID)
            appViewModel.sampleApp= sampleApp
        }
        Log.v("al","canza")
        if(sampleApp.users.size==0)
        {
            startActivity(Intent(this, Splash::class.java))
        }
        appViewModel.sampleApp!!.acquireTokenSilentAsync(SCOPES, sampleApp.users[0], getAuthSilentCallback())
    }

    private fun getAuthSilentCallback(): AuthenticationCallback {
        return object : AuthenticationCallback {
            override fun onSuccess(authenticationResult: AuthenticationResult) {
                /* Successfully got a token, call graph now */
                Log.d(TAG, "Successfully authenticated")
                Log.d("Auth result", authenticationResult.toString())
                appViewModel.setAuthResult(authenticationResult)
            }

            override fun onError(exception: MsalException) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString())
                Toast.makeText(applicationContext, "No hay conexión a internet :/",Toast.LENGTH_LONG).show()
            }

            override fun onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.")
            }
        }
    }

    /* Clears a user's tokens from the cache.
     * Logically similar to "sign out" but only signs out of this app.
     */
    private fun onSignOutClicked() {

        /* Attempt to get a user and remove their cookies from cache */
        NotificationHelper(applicationContext).cancelNotification()

        try {
            var sampleApp = appViewModel.sampleApp

            /* Initialize the MSAL App context */
            if (sampleApp == null) {
                sampleApp = PublicClientApplication(
                        this.applicationContext,
                        CLIENT_ID)
                appViewModel.sampleApp=sampleApp
            }

            val users= appViewModel.sampleApp?.users

            when {
                users == null -> /* We have no users */
                    Toast.makeText(applicationContext, "No se encontró un usuario activo", Toast.LENGTH_SHORT).show()
                users.size == 1 -> {
                    /* We have 1 user */
                    /* Remove from token cache */
                    sampleApp.remove(users[0])
                    Toast.makeText(baseContext, "Sesión cerrada", Toast.LENGTH_SHORT)
                            .show()

                }
                else -> {
                    /* We have multiple users */
                    for (i in users.indices) {
                        sampleApp.remove(users[i])
                    }

                    Toast.makeText(baseContext, "Sesión cerrada", Toast.LENGTH_SHORT)
                            .show()
                }
            }

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()

        } catch (e: MsalClientException) {
            Log.d(TAG, "MSAL Exception Generated while getting users: " + e.toString())

        } catch (e: IndexOutOfBoundsException) {
            Log.d(TAG, "User at this position does not exist: " + e.toString())
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        primeraConexion=false
        manager?.unregisterNetworkCallback(networkCallback)
    }

    fun action(action: String, dialog: BotDialog, wContext:com.ibm.watson.developer_cloud.conversation.v1.model.Context) {
        when {
            action.startsWith(FEED) -> selectFragmentWithDialog(navView?.menu?.getItem(0)!!, dialog)
            action.startsWith(CALENDAR) -> selectFragmentWithDialog(navView?.menu?.getItem(1)!!, dialog)
            action.startsWith(CAMPUS_ACCESS) -> selectFragmentWithDialog(navView?.menu?.getItem(2)!!, dialog)
            else->{
                handleAction(action,dialog,wContext)
                return
            }
        }
        active.postAction(action, wContext)
    }

    private fun handleAction(action: String, dialog: BotDialog, wContext: com.ibm.watson.developer_cloud.conversation.v1.model.Context) {
        if(!action.startsWith(MAIN)){
            return
        }
        val actionValue = action.split(":")[1]
        when(actionValue){
            "dormir"->{
                Log.v("Llega accion", "dormir")
                appViewModel.setSleeping(true)
            }
            "despertar"->{
                Log.v("Llega accion", "despertar")
                appViewModel.setSleeping(false)
            }
            "agregarEvento"->{
                Log.v("Llega accion", "evento")
                Log.v("Llega accion context", wContext.toString())
                val eventDate= createDateFromBot(wContext)
                val endDate= eventDate.clone() as java.util.Calendar
                endDate.add(java.util.Calendar.HOUR_OF_DAY, 1)
                val resultReceiver = object: ResultReceiver(null) {
                    override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
                        if (resultCode == OwoCalendarService.OK_RESULT) {
                            val action = resultData.getString(OwoCalendarService.ACTION)
                            if (action == OwoCalendarService.ADD_EVENT) {
                                onEventAdded()
                            }
                        } else {
                            val errorString = resultData.getString(OwoCalendarService.ERROR_MESSAGE)
                            Log.e("ADD", errorString)
                        }
                    }
                }
                val event = EventModel(title = wContext["Tipo"]as String,
                        description = "Parcial agregado",
                        startDate = eventDate,
                        endDate = endDate)
                this.addEvent(event, resultReceiver)
            }
        }
    }

    private fun createDateFromBot(wContext: com.ibm.watson.developer_cloud.conversation.v1.model.Context): java.util.Calendar{
        val locale= if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) resources.configuration.locales[0] else resources.configuration.locale
        val date= SimpleDateFormat("dd/MM/yyyy", locale).parse(wContext["dateOutput"] as String)
        val resp= java.util.Calendar.getInstance()
        resp.time=date
        resp.set(java.util.Calendar.HOUR_OF_DAY, (wContext["timeOutput"] as String).split(":")[0].toInt())
        resp.set(java.util.Calendar.MINUTE, (wContext["timeOutput"] as String).split(":")[1].toInt())
        return resp
    }

    private fun selectFragmentWithDialog(item: MenuItem, dialog: BotDialog) {
        Log.v("Detacheado", "pre")
        selectFragment(item)
        Log.v("Detacheado", "post")
        active.dialog=dialog
        dialog.setTargetFragment(active,1)
    }

    override fun addEvent(event: EventModel, resultReceiver: ResultReceiver?) {
        val addEventIntent = Intent(this, OwoCalendarService::class.java)
        addEventIntent.action = "com.themelt.owo.services.OwoCalendarService"
        addEventIntent.putExtra(OwoCalendarService.ACTION, OwoCalendarService.ADD_EVENT)
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val accountName = sharedPref.getString(MainActivity.SP_EMAIL_KEY, "")
        addEventIntent.putExtra(OwoCalendarService.ACCOUNT_NAME_PARAM, accountName)
        addEventIntent.putExtra(OwoCalendarService.EVENT, event)
        addEventIntent.putExtra(OwoCalendarService.RESULT_RECEIVER, resultReceiver)
        val s = baseContext.startService(addEventIntent)
        if (s == null) {
            Log.i("ADD", "service not started")
        } else {
            Log.i("ADD", "intent sent")
        }
    }

    override fun onEventAdded() {
        runOnUiThread {
            calendarFragment.updateCursor()
            getRecommendedEvents{ events -> feedFragment.viewAdapter.setData(events)  }
            //if(active is Feed){
            //  getRecommendedEvents{ events -> (active as Feed).viewAdapter.setData(events)  }
            //}
        }
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        // Save the user's current game state
        var tab = 0
        when(active) {
            feedFragment -> tab = 0
            calendarFragment -> tab = 1
            campusFragment -> tab = 2
        }
        savedInstanceState.putInt(CURR_TAB, tab)

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    fun getRecommendedEvents(callback: (events: ArrayList<EventModel>) -> Unit): Boolean {
        if (ContextCompat.checkSelfPermission(applicationContext,
                        Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(applicationContext,
                        Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            return false // false si no llama el intent por no tener permisos
        }
        val addEventIntent = Intent(this, OwoCalendarService::class.java)
        addEventIntent.action = "com.themelt.owo.services.OwoCalendarService"
        addEventIntent.putExtra(OwoCalendarService.ACTION, OwoCalendarService.GET_RECOMMENDED)
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val accountName = sharedPref.getString(MainActivity.SP_EMAIL_KEY, "")
        addEventIntent.putExtra(OwoCalendarService.ACCOUNT_NAME_PARAM, accountName)
        val resultReceiver = object: ResultReceiver(null) {
            override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
                Log.e("GetRecommendedResponder", "Got result: ${resultCode}")
                if (resultCode == OwoCalendarService.OK_RESULT) {
                    val action = resultData.getString(OwoCalendarService.ACTION)
                    Log.e("GetRecommendedResponder", "Action: ${action}")
                    if (action == OwoCalendarService.GET_RECOMMENDED) {
                        Log.e("GetRecommendedResponder", "Got recommended events:")
                        val events = resultData.getParcelableArrayList<EventModel>(OwoCalendarService.RECOMMENDED_RESULT)
                        Log.v("Events",events!!.size.toString())
                        runOnUiThread{
                            callback(events)
                        }
                    }
                } else {
                    val errorString = resultData.getString(OwoCalendarService.ERROR_MESSAGE)
                    Log.e("GetRecommendedResponder", errorString)
                }
            }
        }
        addEventIntent.putExtra(OwoCalendarService.RESULT_RECEIVER, resultReceiver)
        startService(addEventIntent)
        return true // True si puede llamar el intent
        /*
        Ejemplo de uso:
        getRecommendedEvents {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm")
            for (event in it) {
                Log.e("TEST", sdf.format(event.startDate!!.time))
            }
        }
         */
    }
}
