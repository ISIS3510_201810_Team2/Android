package com.themelt.owo.models

import android.os.Parcel
import android.os.Parcelable

data class EventModel(
        var id: Long = -1,
        val title: String = "",
        val description: String = "",
        var calendarId: Long = -1,
        val startDate: java.util.Calendar? = java.util.Calendar.getInstance(),
        val endDate: java.util.Calendar? = java.util.Calendar.getInstance()
        ): Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readSerializable() as? java.util.Calendar,
            parcel.readSerializable() as? java.util.Calendar)

    override fun writeToParcel(p0: Parcel, p1: Int) {
        p0.writeLong(id)
        p0.writeString(title)
        p0.writeString(description)
        p0.writeLong(calendarId)
        p0.writeSerializable(startDate)
        p0.writeSerializable(endDate)
    }


    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EventModel>{
        override fun createFromParcel(p0: Parcel): EventModel {
            return EventModel(p0)
        }

        override fun newArray(p0: Int): Array<EventModel?> {
            return arrayOfNulls<EventModel>(p0)
        }
    }
}