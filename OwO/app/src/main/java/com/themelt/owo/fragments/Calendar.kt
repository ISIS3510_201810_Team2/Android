package com.themelt.owo.fragments

import android.Manifest
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.themelt.owo.R
import com.themelt.owo.bot.BotDialog
import android.provider.CalendarContract
import android.content.*
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.ResultReceiver
import android.os.SystemClock
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.themelt.owo.CALENDAR
import com.themelt.owo.MainFragment
import com.themelt.owo.adapters.EventCursorAdapter
import com.themelt.owo.main.MainActivity
import com.themelt.owo.models.EventModel
import com.themelt.owo.services.OwoCalendarService
import java.text.SimpleDateFormat


class Calendar : MainFragment(), DialogInterface.OnDismissListener   {

    var owoFab: FloatingActionButton?= null

    var openCreateTaskButton: Button?=null

    override val TAG=com.themelt.owo.CALENDAR

    companion object {
        fun newInstance() = Calendar()
        val REQUEST_READ_PERMISSION = 0
        val REQUEST_WRITE_PERMISSION = 1
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var cursorAdapter: EventCursorAdapter

    private var lastButtonMilli = SystemClock.elapsedRealtime()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.calendar_fragment, container, false)
        activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        owoFab= view.findViewById(R.id.owo_fab)

        owoFab?.setOnClickListener {
            openDialog()
            owoFab?.isEnabled= false
        }

        openCreateTaskButton = view.findViewById<Button>(R.id.open_create_task)

        openCreateTaskButton!!.setOnClickListener {
            val currMilli = SystemClock.elapsedRealtime()
            if (currMilli - lastButtonMilli >= 1000) {
                lastButtonMilli = currMilli

                val dialog = CreateTaskDialog()

                val args = Bundle()
                args.putString("initFragment", TAG)
                dialog.arguments = args
                openCreateTaskButton!!.isEnabled= false

                dialog.setTargetFragment(this@Calendar, 1)
                dialog.show(fragmentManager, "Add event Dialog")
                this.fragmentManager?.executePendingTransactions()

                val d = dialog.dialog
                d?.window?.setBackgroundDrawable(null)
            }
        }

        val openCalendarButton =  view.findViewById<Button>(R.id.open_calendar_btn)
        openCalendarButton.setOnClickListener {
            openCalendar()
        }

        cursorAdapter = EventCursorAdapter()
        val linearLayoutManager = LinearLayoutManager(requireActivity())
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView = view.findViewById<RecyclerView>(R.id.events_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = cursorAdapter
        }
        updateCursor()
        askCalendarPermissions()
        return view
    }

    override fun onDismiss(dialog: DialogInterface?) {
        owoFab?.isEnabled= true
        openCreateTaskButton?.isEnabled= true
    }

    fun showCreateTaskDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        val ft = fragmentManager!!.beginTransaction()
        val prev = fragmentManager!!.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)

        // Create and show the dialog.
        val newFragment = CreateTaskDialog.newInstance()
        newFragment.show(ft, "dialog")
    }

    private fun askCalendarPermissions(): Boolean {
        // Here, thisActivity is the current activity
        val act = requireActivity()
        var enters = false
        if (ContextCompat.checkSelfPermission(act,
                        Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(act,
                    arrayOf(Manifest.permission.READ_CALENDAR),
                    REQUEST_READ_PERMISSION);
            enters = true
        }
        if (ContextCompat.checkSelfPermission(act,
                        Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(act,
                    arrayOf(Manifest.permission.WRITE_CALENDAR),
                    REQUEST_WRITE_PERMISSION)
            enters = true
        }
        return enters
    }

    private fun openCalendar() {
        // A date-time specified in milliseconds since the epoch.
        val currMillis = java.util.Calendar.getInstance().timeInMillis
        val builder = CalendarContract.CONTENT_URI.buildUpon()
        builder.appendPath("time")
        ContentUris.appendId(builder, currMillis)
        val intent = Intent(Intent.ACTION_VIEW).setData(builder.build())
        startActivity(intent)
    }

    fun updateCursor() {
        if (askCalendarPermissions()) return
        var cur: Cursor? = null
        val cr = activity?.contentResolver

        val mProjection = arrayOf(
                CalendarContract.Events._ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DESCRIPTION,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND)
        val selection = "(${CalendarContract.Events.DTEND} >= ?)"
        val selectionArgs = arrayOf(java.util.Calendar.getInstance().timeInMillis.toString())
        val uri = CalendarContract.Events.CONTENT_URI
        cur = cr?.query(uri, mProjection,
              selection, selectionArgs,
            "${CalendarContract.Events.DTSTART} ASC")
        if (cur != null) {
            cursorAdapter.setCursor(cur)
        }
    }

    override fun onViewStateRestored(inState: Bundle?) {
        super.onViewStateRestored(inState)
        updateCursor()
    }

    override fun postAction(action: String, wContext: com.ibm.watson.developer_cloud.conversation.v1.model.Context) {
        Log.v("Llega accion 2", action)
        if(action.startsWith(CALENDAR)){
            Log.v("Llega accion 2", "Pre Ejecuta")
            Log.v("Llega accion 2", action.split( ":")[1])
            when(action.split(":")[1]){
                "agregarEvento"->{
                    //TODO Fecha en formato dd<espacio>NombreMes<coma><espacio><año>
                    //TODO Hora en formato hh:mm<espacio><PM/AM>
                    //TODO Tipo es <evento personalizado> <parcial> <taller/tarea> (sí, taller y tarea están juntos con un slash)
                    //TODO FANDIÑO agregarEvento(params[0],params[1],params[2]), en orden es Tipo,fecha,hora
                }
            }

        }
        else{
            owoFab?.isEnabled=true
            (activity as MainActivity).action(action, dialog!!,wContext)
        }
    }

    override fun addMessageFromBot(message: String) {
        dialog?.sendBotMessage(message)
    }

    override fun openDialog() {
        dialog = BotDialog()

        val args = Bundle()
        args.putString("initFragment", TAG)
        dialog?.arguments = args

        dialog?.setTargetFragment(this@Calendar, 1)
        dialog?.show(fragmentManager, "OwO Dialog")
        this.fragmentManager?.executePendingTransactions()

        val d = dialog?.dialog
        d?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}
