package com.themelt.owo.utils

import android.os.ResultReceiver
import com.themelt.owo.models.EventModel

interface OwoEventManager {
    fun onEventAdded()
    fun addEvent(event: EventModel, resultReceiver: ResultReceiver?)
}