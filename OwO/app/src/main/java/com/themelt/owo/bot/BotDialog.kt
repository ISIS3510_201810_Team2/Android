package com.themelt.owo.bot

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

import com.themelt.owo.R
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.app.ActivityCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import com.themelt.owo.AppSubClass
import com.themelt.owo.MainFragment
import java.util.*


class BotDialog : DialogFragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var messages: MutableList<BotMessage>
    lateinit var appViewModel:AppSubClass

    companion object {
        fun newInstance() = BotDialog()
    }

    private lateinit var viewModel: BotDialogViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.v("Deteacheado", "creado")
        val view=  inflater.inflate(R.layout.bot_dialog_fragment, container, false)

        viewManager = LinearLayoutManager(activity!!.applicationContext)

        (viewManager as LinearLayoutManager).orientation = LinearLayoutManager.VERTICAL
        (viewManager as LinearLayoutManager).isSmoothScrollbarEnabled = true

        return view
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        val parentFragment = targetFragment
        if (parentFragment is DialogInterface.OnDismissListener) {
            (parentFragment as DialogInterface.OnDismissListener).onDismiss(dialog)
        }
    }

    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity!!.applicationContext, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.RECORD_AUDIO),
                        3)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            3 -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    dismiss()
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        appViewModel= activity?.run {
            ViewModelProviders.of(this).get(AppSubClass::class.java)
        } ?: throw Exception("Invalid Activity")
        viewModel= ViewModelProviders.of(this).get(BotDialogViewModel::class.java)
        viewModel.setWatsonUsername(appViewModel.getWatsonUsername())
        viewModel.setWatsonPassword(appViewModel.getWatsonPassword())
        viewModel.setWatsonWorkspaceId(appViewModel.getWatsonWorkspaceId())
        viewModel.setSleeping(appViewModel.getSleeping())
        appViewModel.getSleepingObserver().observe(this, Observer<Boolean> { sleeping ->
            viewModel.setSleeping(sleeping)
        })
        messages= viewModel.getMessages().value!!
        if(messages.isEmpty()){
            messages.add(BotMessage("¡Buenos días! Estoy a tu servicio",true))
        }
        Log.v("target", targetFragment.toString())


        val butDelete= view!!.findViewById<ImageButton>(R.id.dialog_delete_button)

        butDelete.setOnClickListener {
            viewModel.deleteMessages()
        }

        viewAdapter = MyAdapter(messages)

        recyclerView = view!!.findViewById<RecyclerView>(R.id.chat_display).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter


        }

        recyclerView.scrollToPosition(messages.size - 1)

        viewModel.getMessages().observe(this, Observer {
            viewAdapter.setData(it!!)
            recyclerView.scrollToPosition(viewAdapter.itemCount-1)
        })

        viewModel.getAccion().observe(this, Observer {
            if(it!=null && !it.isEmpty()){
                Log.v("Llega accion", it)
                (targetFragment as MainFragment).postAction(it,viewModel.context)
            }
        })

        val mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this.context)

        val mSpeechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,Locale.getDefault())

        val editText= view!!.findViewById<EditText>(R.id.bot_input_text)


        val chatMic= view!!.findViewById<CustomImageButton>(R.id.bot_mic_option)


        val chatSend= view!!.findViewById<CustomImageButton>(R.id.bot_send_option)


        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(s.toString().trim().isEmpty() && chatMic.visibility== View.GONE){
                    chatMic.visibility=View.VISIBLE
                    chatSend.visibility=View.GONE

                    val set =  ConstraintSet()
                    val layout = view!!.findViewById(R.id.bot_text_input) as ConstraintLayout
                    set.clone(layout)
                    set.connect(R.id.bot_card_input_text, ConstraintSet.END, R.id.bot_mic_option, ConstraintSet.START, 8)
                    set.applyTo(layout)
                } else if(!s.toString().trim().isEmpty() && chatMic.visibility== View.VISIBLE) {
                    chatMic.visibility=View.GONE
                    chatSend.visibility=View.VISIBLE

                    val set =  ConstraintSet()
                    val layout = view!!.findViewById(R.id.bot_text_input) as ConstraintLayout
                    set.clone(layout)
                    set.connect(R.id.bot_card_input_text, ConstraintSet.END, R.id.bot_send_option, ConstraintSet.START, 8)
                    set.applyTo(layout)
                }
            }
        })


        mSpeechRecognizer.setRecognitionListener(object: RecognitionListener {
            override fun onReadyForSpeech(bundle: Bundle) {

            }

            override fun onBeginningOfSpeech() {

            }

            override fun onRmsChanged(v: Float) {

            }

            override fun onBufferReceived(bytes: ByteArray) {

            }

            override fun onEndOfSpeech() {

            }

            override fun onError(i: Int) {

            }

            override fun onResults(bundle: Bundle) {
                //getting all the matches
                val matches = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)

                //displaying the first match
                if (matches != null){
                    Log.v("a br que sale", matches[0])
                    editText.setText(matches[0])

                }

            }

            override fun onPartialResults(bundle: Bundle) {

            }

            override fun onEvent(i: Int, bundle:Bundle ) {

            }
        })


        val vibe =  activity!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        chatMic.setOnTouchListener { _, event ->
            checkPermission()

            when (event?.action) {
                MotionEvent.ACTION_DOWN ->{
                    Log.v("llega", "usr chat")
                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent)
                    if (Build.VERSION.SDK_INT >= 26) {
                        vibe.vibrate(VibrationEffect.createOneShot(80,10));
                    } else {
                        vibe.vibrate(80)
                    }
                }
                MotionEvent.ACTION_UP ->{
                    Log.v("sale", "usr chat")
                    mSpeechRecognizer.stopListening()
                    if (Build.VERSION.SDK_INT >= 26) {
                        vibe.vibrate(VibrationEffect.createOneShot(80,10));
                    } else {
                        vibe.vibrate(80)
                    }
                }
            }

            false
        }

        chatSend.setOnClickListener{
                Log.v("sera que llega", "no c")
                val textToSend= editText.text.toString()
                if(!textToSend.trim().isEmpty())
                {
                    sendInteractiveMessage(editText.text.toString())
                    editText.setText("")
                }
        }
    }

    fun sendInteractiveMessage(message: String) {
        getViewModel().addUserMessage(message)
    }

    fun sendBotMessage(message: String) {
        getViewModel().addBotMessage(message)
    }

    fun getViewModel(): BotDialogViewModel{
        if (!::viewModel.isInitialized) {
            viewModel = ViewModelProviders.of(this).get(BotDialogViewModel::class.java)
        }
        return viewModel
    }

    override fun onDetach() {
        super.onDetach()
        Log.v("Detacheado", "El bot 3")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v("Detacheado", "El bot 2")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.v("Detacheado", "El bot 1")
    }

    override fun onPause() {
        super.onPause()
        Log.v("Detacheado","Al menos")
    }
}

class BotMessage constructor(val message:String,val isBot: Boolean) {
    var loading=false
    fun setLoading(): BotMessage {
        loading=true
        return this
    }
}

class MyAdapter(private var myDataset: MutableList<BotMessage>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.

    class MyViewHolderBot(v: View) : RecyclerView.ViewHolder(v) {
        // each data item is just a string in this case
        var mTextView: TextView = v.findViewById(R.id.text_msg)

    }


    class MyViewHolderUser(v: View) : RecyclerView.ViewHolder(v) {
        // each data item is just a string in this case
        var mTextView: TextView = v.findViewById(R.id.text_msg)

    }

    class MyViewHolderLoading(v: View) : RecyclerView.ViewHolder(v)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            0 ->{
                val v = LayoutInflater.from(parent.context)
                        .inflate(R.layout.bot_left_chat_bubble, parent, false)
                MyViewHolderBot(v)
            }
            1-> {
                val v = LayoutInflater.from(parent.context)
                        .inflate(R.layout.bot_right_chat_bubble, parent, false)
                MyViewHolderUser(v)
            }
            else-> {
                val v = LayoutInflater.from(parent.context)
                        .inflate(R.layout.bot_left_loading_bubble, parent, false)
                MyViewHolderLoading(v)
            }
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType){
            0 ->{
                (holder as MyViewHolderBot).mTextView.text = myDataset[position].message
            }
            1 ->{
                (holder as MyViewHolderUser).mTextView.text = myDataset[position].message
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(myDataset[position].loading) return 2
        return when(myDataset[position].isBot)
        {
            true-> 0
            false-> 1
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size

    fun setData(newData: MutableList<BotMessage>) {
        myDataset=newData
        notifyDataSetChanged()
    }
}